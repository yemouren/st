#+TITLE: My Build of Simple Terminal

基于 [[github://LukeSmithxyz/st.git][Luke's build of st]]

* 补丁：
- solarized(branch solarized)
* 安装
#+begin_src shell
$ git clone https://github.com/yepengdroid/st.git
$ cd st
$ sudo make clean install
#+end_src
使用tmux可能会在使用vim编辑 =.tmux.conf= 时崩溃
